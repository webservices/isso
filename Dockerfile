FROM alpine:3.5
MAINTAINER web-services-core, <web-services-core@cern.ch>

RUN apk upgrade --update \
 && apk add python3 python3-dev sqlite gcc musl-dev ca-certificates openssl \
 && update-ca-certificates \
 && pip3 install --upgrade pip \
 && pip3 install isso gunicorn gevent \
 && mkdir -p /etc/isso.d \ 
 && mkdir -p /db \ 
 && chmod -R g+rw /db

VOLUME ["/db"]

COPY conf /etc/isso.d/

COPY run.sh /run.sh
RUN chmod +x /run.sh

EXPOSE 8080

ENTRYPOINT ["/run.sh"]