# Deploy
- `oc new-app https://:@gitlab.cern.ch:8443/webservices/isso.git ISSO_SETTINGS='/etc/isso.d/test-iposadat.conf;'`
- `oc expose svc/isso`
- Then on Openshift web portal, go to Applications menu > routes > click on the route > edit > set secure > Edge and Redirect insecure traffic.